const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: {
        home: path.join(__dirname, '../src/index.tsx'),
    },

    output: {
        filename: '[name].js',
        path: path.join(__dirname, '../dist'),
        publicPath: '/'
    },

    module: {
        rules: [

            // typescript
            {
                test: /.(ts|tsx)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-react',
                            '@babel/preset-typescript'
                        ]
                    }
                }
            },

            // css
            {
                test: /.css$/,
                use: ['style-loader', 'css-loader']
            },

            // images
            {
                test: /.(png|jpg|jpeg|gif|svg)$/,
                type: 'asset'
            }
        ]
    },

    resolve: {
        extensions: ['.js', '.tsx', '.ts'],
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, '../public/index.html'),
            inject: true,
        })
    ]


}