const path = require("path");
const nodeExternals = require("webpack-node-externals");

module.exports = {
    target: "node",
    mode: 'production',
    entry: path.join(__dirname, '../src/server/index.js'),
    output: {
        filename: "server.js",
        path: path.resolve(__dirname, "../dist"),
    },
    externals: [nodeExternals()],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                options: {
                    presets: [
                        '@babel/preset-react',
                    ]
                }
            },

            // typescript
            {
                test: /.(ts|tsx)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-react',
                            '@babel/preset-typescript'
                        ]
                    }
                }
            },

            // css
            {
                test: /.css$/,
                use: ['style-loader', 'css-loader']
            },

            // images
            {
                test: /.(png|jpg|jpeg|gif|svg)$/,
                type: 'asset'
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.tsx', '.ts'],
    },
}

