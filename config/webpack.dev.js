const path = require('path')
const { merge } = require('webpack-merge')
const baseConfig = require('./webpack.base.js')

const devConfig = {
    mode: 'development',
    devtool: 'eval-cheap-module-source-map', // ???
    devServer: {
        port: 7777,
        compress: false,   // for dev, imporve hot reload speed
        hot: true,
        historyApiFallback: true, // ???
        static: {
            directory: path.join(__dirname, "../public")
        },
        open: true
    }
}

module.exports = merge(baseConfig, devConfig)