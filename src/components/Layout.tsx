import React, { PropsWithChildren } from 'react'
import { useNavigate, useLocation } from 'react-router-dom'

import '../styles/layout.css'
import IMenu from '../models/Menu'

const Layout: React.FC<PropsWithChildren> = ({ children }) => {

    const menus: Array<IMenu> = [
        {
            text: 'home',
            path: '/'
        },
        {
            text: 'product',
            path: '/product'
        },
        {
            text: 'cart',
            path: '/cart'
        },
        {
            text: 'admin-product',
            path: '/admin/product'
        },
        {
            text: 'admin-products',
            path: '/admin/products'
        },
    ]



    return (
        <>
            <div className="sidenav">
                {
                    menus.map(menu => {
                        return (
                            <a key={Math.random()} href={menu.path}>{menu.text}</a>
                        )
                    })
                }
            </div>
            <div className="content">
                <div className="header">
                    <h1>title</h1>
                </div>
                {children}
            </div>
        </>
    )
}

export default Layout