import React, { useState } from 'react'
import { useAppDispatch, useAppSelecter } from '../store/hooks'
import { increment, amountAdd } from '../features/counter/counter-slice'

const Home = () => {

    const count = useAppSelecter(state => state.counter.value);
    const dispatch = useAppDispatch();

    const handleClick = () => {
        dispatch(amountAdd(10))
    }

    return (
        <div>
            <div>
                <button onClick={handleClick}>Count is : {count}</button>
            </div>
        </div>

    )
}

export default Home