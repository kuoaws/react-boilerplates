interface IMenu {
    text: string;
    path: string;
}

export default IMenu